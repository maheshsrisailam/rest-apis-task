const isValidName = (name) => {
    const regex = /^[a-zA-Z ]{2,30}$/;
    return regex.test(name)
}

const isValidEmail = (email) => {
    const regex = /^[a-z0-9]+@[a-z]+\.[a-z]{2,3}$/;
    return regex.test(email)
}

const isValidMObileNumber = (mobilenumber) => {
    const regex = /^\d{10}$/;
    return regex.test(mobilenumber)
}

const isValidPassword = (password) => {
    return  ((password.length >= 5) && (password.length <= 256)) 
}

module.exports = {isValidName,isValidEmail,isValidMObileNumber,isValidPassword}