const dotenv = require('dotenv')
const express = require('express')
const router = express.Router()

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const logger = require('../logger.js')

const dbConnection = require('../databaseConnection')
const authenticationMiddleware = require('../middleware.js')
const {isValidName,isValidEmail,isValidPassword} = require('../validations')

dotenv.config()


//Sign Up or Register API

router.post('/signup', async (request,response) => {
    try {
        const {firstname,lastname,email,password} = request.body
        if (!isValidName(firstname)) {
            response.status(400).send(`Invalid firstname. It should contains only alphabets.`)
        } else if (!isValidName(lastname)) {
            response.status(400).send(`Invalid lastname. It should contains only alphabets.`)
        } else if (!isValidEmail(email)){
            response.status(400).send(`Invalid EMAIL:- ${email}`)
        } else if (!isValidPassword(password)) {
            response.status(400).send(`Characters in the password must be lies between 6 to 256`)
        } else {
            const hashedPassword = await bcrypt.hash(password,10)
            const gettingAdminDetails = `SELECT email FROM admins where email = "${request.body.email}";`
            dbConnection.query(gettingAdminDetails,(error,data)=>{
                if (error) response.status(500).send(error.message)
                if (data.length === 0) {
                    const insertingData = `INSERT INTO admins (firstname,lastname,email,password) values ("${firstname}","${lastname}","${email}","${hashedPassword}")`
                    dbConnection.query(insertingData,(error) => {
                        if (error) response.status(500).send(error.message)
                            response.status(201).json({message:`Admin with EMAIL : ${email} is created successfully.` })
                    })
                } else {
                    response.status(401).json({message: `Admin with EMAIL : ${email} is already exists.`})
                }
            })
        }
        
    }catch (error) {
        response.status(500).send(error.message)
    }
});


//Login API

router.post('/login',async (request,response) => {
    try {
        const {email,password} = request.body
        const getAdminDetailsQuery = `SELECT * FROM admins where email="${email}";`
        
        dbConnection.query(getAdminDetailsQuery, async (error, data)=>{
            if (error) response.status(500).send(error.message)
            console.log(data)
            if (data.length !== 0) {
                const payload = {
                    email:email
                }
                const isPasswordMatched = await bcrypt.compare(password, data[0].password)
                if (isPasswordMatched) {
                    const jwtToken = generateAccessToken(payload)
                    const refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN)
                    dbConnection.query(`INSERT INTO refreshtokens VALUES ("${email}","${refreshToken}")`)
                    response.status(201).json({"JWT_TOKEN": jwtToken,"REFRESH_TOKEN":refreshToken})
                } else {
                    response.status(401).json({message: "Invalid Password"})
                }
            } else {
                response.status(404).json({message: `No records found with this EMAIL: ${email}`})
            }
        })
    } catch (error) {
        response.status(500).send(error.message)
    }
})

function generateAccessToken(payload) {
    return jwt.sign(payload, process.env.SECRET_TOKEN,{expiresIn:"15m"})
}

//API to generate new access token

router.post('/token',(request,response)=>{
    try {
        const refreshToken = request.body.token
        if (refreshToken === null) {
            logger.error("Unauthorized User")
            response.status(401).send("Unauthorized user")
        } else {
            dbConnection.query(`SELECT email from refreshtokens where token = "${refreshToken}";`,(error,data) => {
                if (error) response.status(500).send(error.message)
                if (data.length!==0) {
                    jwt.verify(refreshToken,process.env.REFRESH_TOKEN, (error,payload)=>{
                        if (error) {
                            return response.status(403).send("Invalid Refresh Token")
                        }
                        const accessToken = generateAccessToken({email:payload.email})
                        logger.info("New Access Token Generated.")
                        response.send({accessToken})
                    })
                } else {
                    response.status(404).send("Refresh token is not found in the database.")
                }
                
            })
        }
    } catch (error) {
        response.status(500).send(error.message)
    }
});

//API to read the data from the database

router.get('/employees',authenticationMiddleware,(request,response) => {
    try {
        const getEmployeeQuery = "SELECT * FROM employees"
        dbConnection.query(getEmployeeQuery,(error,data)=>{
            (error) ? response.status(500).send(error.message) : response.status(200).send(data)
        })
    } catch (error) {
        response.status(500).send(error.message)
    }
});

//API to update the data from the database

router.put('/employees/:id', authenticationMiddleware, (request,response) => {
    try {
        const {name,designation,company_name,email,mobile_no,address,company_location} = request.body
        const getEmployeeQuery = `SELECT * FROM employees where id = ${request.params.id};`
        dbConnection.query(getEmployeeQuery,(error,data) => {
            if (error) response.status(500).send(error.message)
            if (data.length === 0) {
                response.status(404).send(`Data is not found with this ID: ${request.params.id}`)
            } else {
                const updatingEmployeeDetials = `UPDATE employees
                    SET 
                        name= "${name}",
                        designation="${designation}",
                        company_name="${company_name}",
                        email="${email}",
                        mobile_no = "${mobile_no}",
                        address="${address}",
                        company_location="${company_location}"
                    where id = ${request.params.id};`
                dbConnection.query(updatingEmployeeDetials,(error) => {
                    (error) ? logger.error(error.message) : response.status(200).send(`Employee with ID: ${request.params.id} is updated successfully.`)
                })
            }
        })
    } catch (error) {
        response.status(500).send(error.message)
    }
})

//API to delete the data from the database

router.delete('/employees/delete/:id', authenticationMiddleware, (request,response) => {
    try {
        const getEmployeeQuery = `SELECT * FROM employees where id = ${request.params.id};`
        dbConnection.query(getEmployeeQuery,(error,data) => {
            if (error) response.status(500).send(error.message)
            if (data.length === 0) {
                response.status(404).send(`Data is not found with this ID: ${request.params.id}`)
            } else {
                const deletingEmployeeQuery = `DELETE FROM employees
                    where id = ${request.params.id};`
                dbConnection.query(deletingEmployeeQuery,(error) => {
                    (error) ? response.status(500).send(error.message) : response.status(200).send(`Employee with ID: ${request.params.id} is deleted.`)
                })
            }
        })

    } catch (error) {
        response.status(500).send(error.message)
    }
});

//API to logout from the application

router.delete('/logout', authenticationMiddleware, (request,response) => {
    try {
        const {email} = request
        
        const getEmailQuery = `SELECT email FROM refreshtokens WHERE email = "${email}";`
        dbConnection.query(getEmailQuery,(error,data) => {
            logger.info(email)
            if (error) response.status(500).send(error.message)
            if (data.length === 0) {
                response.status(404).send(`Data is not found regarding with EMAIL: ${email}`)
            } else {
                const deletingRefreshTokenQuery = `DELETE FROM refreshtokens where email = "${email}";`
                dbConnection.query(deletingRefreshTokenQuery, (error) => {
                    (error) ? response.status(500).send(error.message) : response.status(200).send("Logout Successfull") 
                })
            }
        })
    } catch (error) {
        response.status(500).send(error.message)
    }
})

module.exports = router;