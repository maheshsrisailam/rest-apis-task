const mysql = require('mysql');
const logger = require('./logger');

const dbConnection = mysql.createConnection({
    host:"db4free.net",
    user:"mahesh",
    password:"Mahesh@7777",
    database: "entitiesdatabase"
});

dbConnection.connect((error)=>{
    if (error) return logger.error(error.message)
    logger.info("Database connected successfully.") 
});

module.exports = dbConnection;