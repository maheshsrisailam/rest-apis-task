
POST http://localhost:5000/admins/signup
Content-Type: application/json

{
    "firstname": "Mahesh",
    "lastname" : "Srisailam",
    "email" : "mahesh@gmail.com",
    "password" : "mahesh123"
}

###
POST http://localhost:5000/admins/login 
Content-Type: application/json

{
    "email":"mahesh@gmail.com",
    "password" : "mahesh123"
}

###

GET http://localhost:5000/admins/employees
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1haGVzaEBnbWFpbC5jb20iLCJpYXQiOjE2NzA1OTk3MjcsImV4cCI6MTY3MDYwMDYyN30.34HuaHAblDu-HecZgct82jC-sbFD8jomaEsuVTXGvIg
###

POST http://localhost:5000/admins/token
Content-Type: application/json

{
    "token" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1haGVzaEBnbWFpbC5jb20iLCJpYXQiOjE2NzA1OTk3Mjd9.TYf3tfse8-oSiePWhQMXld9qhy06GXRFCTXc1He5iXQ"
}

###
PUT http://localhost:5000/admins/employees/4
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1haGVzaEBnbWFpbC5jb20iLCJpYXQiOjE2NzA1ODU2NjMsImV4cCI6MTY3MDU4NjU2M30.AzRVhmY9C8lHm36Vwo33h3H8ZP7KIxs-mTIcn-PWKX4

{
    "name" : "Khaaja Nawaz",
    "designation" : "Full Stack Developer",
    "company_name" : "Cognizant",
    "email" : "nawaz@gmail.com",
    "mobile_no" : 9998745632,
    "address" : "Hyderabad",
    "company_location" : "Hyderabad"
}

###
DELETE http://localhost:5000/admins/employees/delete/1
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImRpbmVzaEBnbWFpbC5jb20iLCJpYXQiOjE2NzA1ODExMDksImV4cCI6MTY3MDU4MTE1NH0.5hjPEd5xfqImPkSKJcGuyqP1YJS3vHb2Ll5FSD_Q6S0

###
DELETE http://localhost:5000/admins/logout
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1haGVzaEBnbWFpbC5jb20iLCJpYXQiOjE2NzA2NjUyMzEsImV4cCI6MTY3MDY2NjEzMX0.2UGx58sjh9la2Xa2gPrE1V4PK9H8kOXQijZkCw4VTj4