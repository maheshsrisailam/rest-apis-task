const express = require('express')
const cors = require('cors')
const logger = require('./src/logger')

const router = require('./src/routes/apis')

const app = express()

app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.use(cors())

app.use('/admins', router)

app.use((request,response,next) => {
    const error = new Error("NOT FOUND")
    error.status(404)
    next(error)
})

app.use((error,request,response,next) => {
     response.status(404 || 500).send(error.message)
});

const PORT = process.env.PORT || 5000

app.listen(PORT,()=>logger.info(`Server started listening on PORT : ${PORT}`));